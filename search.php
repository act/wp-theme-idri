<?php get_header(); ?>

	<style type="text/css">
		#menu-item-635 a {color: #434240; border-bottom: 4px solid #fff;}
		#menu-item-635:hover a {color: #265a8c; border-bottom: 4px solid #265a8c;}
		#menu-item-566 a {border-left: 5px solid #fff; border-left: 0.5rem solid #fff;}
	</style>

	<?php

		global $query_string;
		query_posts( $query_string . '&posts_per_page=-1' );

        $title = get_the_title(9);
        $content = get_post_field('post_content', 9);

		if (have_posts()) { 
	?>
		<section class="page-section narrow">
			<section class="container ultra">

				<?php 
					global $query_string;
					query_posts( $query_string . '&posts_per_page=-1' );
					$search_query = get_search_query();
					if (have_posts()) {
				?>
					<h1 class="page-title alt green small left">Search Results For: '<?php echo $search_query; ?>'</h1>
					<ul class="search-results-list">
						<?php while ( have_posts() ) : the_post(); ?>
							<li>
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<?php the_excerpt(); ?>
								<a href="<?php the_permalink(); ?>" class="button small-button">View</a>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php } else { ?>
					<p>Sorry, your search didn't return any results</p>
				<?php } ?>

			</section>
		</section>

	<?php } ?>

<?php get_footer(); ?>
