<?php 
	$postslist = get_posts('numberposts=6');
?>

<?php if($postslist) { ?>
	<section class="page-section mid">
		<section class="container ultra no-top">
			<div class="post-list">
				<?php foreach ($postslist as $post) { ?>
					<?php setup_postdata($post); ?>
						<div class="post-excerpt">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?></a>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</div>
					<?php wp_reset_postdata(); ?>	
				<?php } ?>
			</div>
		</section>
	</section>
<?php } ?>
