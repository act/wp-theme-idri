<?php
/**
 * IDRI Theme Customizer
 *
 */

 

add_action( 'customize_register', 'idri_customize_register' );
function idri_customize_register( $wp_customize ) {
	/******************************************************************************
	 * Add postMessage support for site title and description for the Theme Customizer.
	 */
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'reading_research_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'reading_research_customize_partial_blogdescription',
		) );
	}
	




	//******************************************************************************
	//Adding custom logo
   $wp_customize->add_setting('custom_logo', array(
		'default-image' => get_template_directory_uri() . '',
		'transport'     => 'refresh',
		'height'        => 250,
		'width'        => 250,
		'capability' => 'edit_theme_options',
	));
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'idri_logo_control', array(
		'label' => 'Custom logo',
		'section' => 'title_tagline',
		'priority'	=>	'1',
		'settings' => 'custom_logo',
		'description' => 'Displayed in top left',
	)));
	
	//******************************************************************************
	//Adding custom phone number (top right)
   $wp_customize->add_setting('custom_phone', array(
		'default-text' => '',
		'transport'     => 'refresh',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'wp_filter_nohtml_kses' //removes all HTML from content
	));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'idri_phone_control', array(
		'label' => 'Phone number',
		'section' => 'title_tagline',
		'priority'	=>	'29',
		'settings' => 'custom_phone',
		'description' => 'Phone number displayed in top bar',
	)));	
	
	//******************************************************************************
	//Adding custom email (top right)
   $wp_customize->add_setting('custom_email', array(
		'default-text' => '',
		'transport'     => 'refresh',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'wp_filter_nohtml_kses' //removes all HTML from content
	));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'idri_email_control', array(
		'label' => 'Contact email',
		'section' => 'title_tagline',
		'priority'	=>	'30',
		'settings' => 'custom_email',
		'description' => 'Email link displayed in top bar',
	)));
	
	//******************************************************************************
	//Adding custom twitter (top right)
   $wp_customize->add_setting('custom_twitter', array(
		'default-text' => '',
		'transport'     => 'refresh',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'wp_filter_nohtml_kses' //removes all HTML from content
	));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'idri_twitter_control', array(
		'label' => 'Twitter link',
		'section' => 'title_tagline',
		'priority'	=>	'31',
		'settings' => 'custom_twitter',
		'description' => 'Twitter link displayed in top bar (e.g. https://twitter.com/UniRdg_HCIC)',
	)));	

	//******************************************************************************
	//Adding custom copyright
   $wp_customize->add_setting('custom_copyright', array(
		'default-text' => '',
		'transport'     => 'refresh',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'wp_filter_nohtml_kses' //removes all HTML from content
	));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'idri_copyright_control', array(
		'label' => 'Site copyright',
		'section' => 'title_tagline',
		'priority'	=>	'32',
		'settings' => 'custom_copyright',
		'description' => 'Copyright text displayed in footer',
	)));
	
	
	//******************************************************************************
	//Adding selection of theme color	 
	$wp_customize->add_setting('idri_color', array(
		'default'  	=> '#00aeef',
		'sanitize_callback' => 'sanitize_hex_color',
	 ));
	/*$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'idri_color_control', array(
	 'label'   => 'Theme color',
	 'section' => 'colors',
	 'settings' => 'idri_color',
	)));*/
	$wp_customize->add_control('idri_color_control', array(
	 'label'   => 'Theme color',
	 'section' => 'colors',
	 'settings' => 'idri_color',
	 'type'    => 'select',
	 'choices'    => array(
				'#ef7945' => 'Orange',
				'#8abd24' => 'Green',
				'#00aeef' => 'Blue',
				'#e6007e' => 'Magenta',
				'#2a377e' => 'Navy',
				'#d61b26' => 'Red',
			),
	));
	
	//******************************************************************************
	//Adding selection of background color	 
	$wp_customize->add_setting('idri_bg_color', array(
		'default'  	=> '#FFF',
		'sanitize_callback' => 'sanitize_hex_color',
	 ));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'idri_bg_color_control', array(
	 'label'   => 'Background color',
	 'section' => 'colors',
	 'settings' => 'idri_bg_color',
	)));
	
	
	//******************************************************************************
	//Adding archive banner (for posts)	
	$wp_customize->add_section( 'banners', array(
	  'priority'       => 121,
	  'capability'     => 'edit_theme_options',
	  'theme_supports' => '',
	  'title'          => 'Banners',
	) );
   $wp_customize->add_setting('idri_archive_banner', array(
		'default-image' => get_template_directory_uri() . '',
		'transport'     => 'refresh',
		'height'        => 401,
		'width'        => 1276,
		'capability' => 'edit_theme_options',
	));
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'idri_archive_banner_control', array(
		'label' => 'Posts banner picture',
		'section' => 'banners',
		'priority'	=>	'1',
		'settings' => 'idri_archive_banner',
		'description' => 'Banner displayed when visualizing blog posts lists (advised dimention: 1280px * 400 px)',
	)));	
	
	
	//******************************************************************************
	//Adding Google Maps API key
	$wp_customize->add_section( 'google_key', array(
	  'priority'       => 122,
	  'capability'     => 'edit_theme_options',
	  'theme_supports' => '',
	  'title'          => 'Google maps API key',
	) );
   $wp_customize->add_setting('idri_google_key', array(
		'default' => '',
		'transport'     => 'refresh',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'wp_filter_nohtml_kses' //removes all HTML from content
	));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'idri_google_key_control', array(
		'label' => 'Google Maps API Key',
		'section' => 'google_key',
		'priority'	=>	'30',
		'settings' => 'idri_google_key',
		'description' => 'Put here your google maps API key to make the "maps" block work. See <a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank">https://developers.google.com/maps/documentation/javascript/get-api-key</a>',
	)));
}


