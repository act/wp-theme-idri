<?php

/*********************************************************
 * Customizer additions.
**********************************************************/
require get_stylesheet_directory() . '/customizer.php';



/*********************************************************
  Default theme functions
**********************************************************/
	 
	add_action( 'after_setup_theme', 'wedo_theme_setup' );
	function wedo_theme_setup() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_action( 'widgets_init', 'wedo_register_sidebars' );
		add_filter('wp_mail_from_name', 'new_mail_from_name');
		add_action( 'init', 'register_my_menus' );
		add_action( 'login_enqueue_scripts', 'my_login_logo' );
		add_action('login_head', 'add_favicon');
		add_action('admin_head', 'add_favicon');
		add_action('init', 'wedo_posts');
		add_action('init', 'wedo_taxonomies');
		add_action('init', 'flush_rewrite_rules');
		add_action( 'wp_enqueue_scripts', 'wedo_scripts' );
		add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);
		add_filter( 'wpseo_metabox_prio', function() { return 'low';});
		add_image_size( 'small-square', 200, 200, true );
		add_image_size( 'square', 400, 400, true );
		add_image_size( 'post-feature', 900, 450, true );
	}

	get_template_part('functions/include', 'favicons');
	get_template_part('functions/include', 'postnav');
	get_template_part('functions/include', 'menus');
	get_template_part('functions/include', 'scripts');
	get_template_part('functions/include', 'sidebar');
	get_template_part('functions/include', 'cpts');
	get_template_part('functions/include', 'users');
	get_template_part('functions/include', 'email');
	get_template_part('functions/include', 'footer');
	get_template_part('functions/include', 'gallery'); 
	get_template_part('functions/include', 'search'); 

	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

	add_filter('acf/settings/path', 'my_acf_settings_path');
	function my_acf_settings_path( $path ) {
	 
	    // update path
	    $path = get_stylesheet_directory() . '/acf/';
	    
	    // return
	    return $path;
	    
	}
	add_filter('acf/settings/dir', 'my_acf_settings_dir');
	function my_acf_settings_dir( $dir ) {
	    $dir = get_stylesheet_directory_uri() . '/acf/';
	    return $dir;
	}
	include_once( get_stylesheet_directory() . '/acf/acf.php' );
	add_filter('acf/settings/google_api_key', function () {
		echo "google key: ".get_theme_mod('google_key');
	    return get_theme_mod('google_key');
	});

	function wpb_set_post_views($postID) {
	    $count_key = 'wpb_post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        $count = 0;
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	    } else {
	        $count++;
	        update_post_meta($postID, $count_key, $count);
	    }
	}
	//To keep the count accurate, lets get rid of prefetching
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	function wpb_track_post_views ($post_id) {
	    if ( !is_single() ) return;
	    if ( empty ( $post_id) ) {
	        global $post;
	        $post_id = $post->ID;    
	    }
	    wpb_set_post_views($post_id);
	}
	add_action( 'wp_head', 'wpb_track_post_views');

	function wpb_get_post_views($postID){
	    $count_key = 'wpb_post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	        return "0 View";
	    }
	    return $count.' Views';
	}

/*********************************************************
  USE CUSTOMISER COLORS
**********************************************************/
function theme_enqueue_styles() {
  wp_enqueue_style( 'theme-styles', get_stylesheet_uri() ); // This is where you enqueue your theme's main stylesheet
  $custom_css = theme_get_customizer_css();
  wp_add_inline_style( 'theme-styles', $custom_css );
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function theme_get_customizer_css() {
    ob_start();

    $theme_color = get_theme_mod( 'idri_color', '' );
    if ( ! empty( $theme_color ) ) {
      ?>
		::-moz-selection {
			background: <?php echo $theme_color; ?>;
		}

		::selection {
			background: <?php echo $theme_color; ?>;
		}

		::-moz-selection {
			background: <?php echo $theme_color; ?>;
		}
		a {
			color: <?php echo $theme_color; ?>;
		}
		.title-block {
			border-bottom: 1px solid <?php echo $theme_color; ?>
		}

		.title-block h1 {
			color: <?php echo $theme_color; ?>;
		}

		.page-section h1, article h1 {
			color: <?php echo $theme_color; ?>;
		}
		.post-excerpt-content h2 {
			border-bottom: 1px solid <?php echo $theme_color; ?>;
		}
		.widget h2 {
			color: <?php echo $theme_color; ?>;
		}
		p.post-meta span i {
			color: <?php echo $theme_color; ?>;
		}
		header .header-meta {
			background: <?php echo $theme_color; ?>;
		}
		footer .footer-main {
			background: <?php echo $theme_color; ?>;
		}
		header nav ul li a {
			border-top: none;
		}
		header nav ul li:hover>a,
		header nav ul li.current-page-ancestor>a,
		header nav ul li.current_page_item>a,
		header nav ul li.current_page_parent>a,
		header nav ul li.current-menu-item>a {
			border-bottom: 4px solid <?php echo $theme_color; ?>;
			color: <?php echo $theme_color; ?>;
		}
		header nav ul li>ul {
			background: <?php echo $theme_color; ?>;
		}
		header nav ul li>ul li a {
			border-left: 5px solid <?php echo $theme_color; ?>;
		}
		@media only screen and (max-width: 31em) {
			.mobile-nav-trigger {
				background: <?php echo $theme_color; ?>;
			}
		}
		.mobile-nav li:hover a,
		.mobile-nav li.current-page-ancestor a,
		.mobile-nav li.current_page_item a,
		.mobile-nav li.current_page_parent a,
		.mobile-nav li.current-menu-item a {
			border-left: 5px solid <?php echo $theme_color; ?>;
			border-left: 0.5rem solid <?php echo $theme_color; ?>;
		}
		.filter-block {
			border-top: 1px solid <?php echo $theme_color; ?>;
			border-bottom: 1px solid <?php echo $theme_color; ?>;
		}
		.filter-block ul li a {
			color: <?php echo $theme_color; ?>;
		}

		.filter-block ul li a.active,
		.filter-block ul li a:hover {
			border-bottom: 1px solid <?php echo $theme_color; ?>;
		}
		.pagination ul li:hover a,
		.pagination ul li.active a {
			background: <?php echo $theme_color; ?>;
		}
		.blog-list .cat-list a {
			background: <?php echo $theme_color; ?>;
		}
		.research-grid>li a .research-overlay {
			background: <?php echo $theme_color; ?>;
		}
		.publication-list .widget-body ul li {
			border: 1px solid <?php echo $theme_color; ?>;
		}

		.publication-list .widget-body ul li a {
			color: <?php echo $theme_color; ?>;
		}
      <?php
    }
	
	$bg_color = get_theme_mod( 'idri_bg_color', '' );
    if ( ! empty( $bg_color ) ) { ?>
		body {
			background: <?php echo $bg_color; ?>;
		}
	<?php
	}

    $css = ob_get_clean();
    return $css;
}