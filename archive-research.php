<?php get_header(); ?>

	<style type="text/css">
		#menu-item-635 a {color: #434240; border-bottom: 4px solid #fff;}
		#menu-item-635:hover a {color: #265a8c; border-bottom: 4px solid #265a8c;}
		#menu-item-566 a {border-left: 5px solid #fff; border-left: 0.5rem solid #fff;}
	</style>

	<?php 
		global $query_string;
		//line hidden by E. Mathieu on 11/07/2018 to make it work on homepage:   query_posts( $query_string . '&posts_per_page=-1' );

		$taxonomy = 'research-category';
        $tax_terms = get_terms($taxonomy);

        $title = get_the_title(537);
        $content = get_post_field('post_content', 537);

		if (have_posts()) { 
	?>

		<?php /* Hidden this reference to post 537. Hard coded and not usable for other sites!
		if($content) { ?>
			<section class="page-section mid">
				<section class="container ultra">
					<?php echo $content; ?>
				</section>
			</section>
		<?php } */?>

		<section class="page-section standard">
			<section class="container ultra">

				<?php if($tax_terms) { ?>
					<section class="filter-block">
						<ul class="research-filters">
							<li class="">Filter by:</li>
							<li><a href="" class="filter active" data-filter=".all">All</a></li>
							<?php foreach ($tax_terms as $tax_term) { ?>
				        		<li>
				        			<a href="" class="filter" data-filter=".<?php echo $tax_term->slug; ?>"><?php echo $tax_term->name; ?></a>
				        		</li>
							<?php } ?>
						</ul>
					</section>
				<?php } ?>

				<ul class="research-grid">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part('includes/partial', 'research'); ?>
					<?php endwhile; ?>
				</ul>

			</section>
		</section>



	<?php } ?>

<?php get_footer(); ?>
