			<footer>
				<section class="footer-main page-section standard">
					<section class="container main">
						<?php if(get_field('footer_columns', 6)) { ?>
							<section class="three-col">
								<?php while(the_repeater_field('footer_columns', 6)) { ?>
									<aside>
										<h2><?php the_sub_field('title', 6); ?></h2>
										<p><?php the_sub_field('content', 6); ?></p>
									</aside>
								<?php } ?>
							</section>
						<?php } ?>
					</section>
				</section>
				<section class="footer-meta">
					<section class="container">
						<p>&copy; <?php echo date("Y"); ?> <?php echo get_theme_mod( 'custom_copyright', 'University of Reading' ); ?></p>
					</section>
				</section>
			</footer>
		</section>
	</section>

	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?php echo get_theme_mod('google_key');?>"></script>	
	<script src="<?php echo get_template_directory_uri(); ?>/js/lib/modernizr-custom.js" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/lib/matchMedia.js" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/lib/matchMedia.addListener.js" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/lib/enquire.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/font-awesome/css/font-awesome.min.css">
	<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
	
	<!--Google Analytics-->

	<?php wp_footer(); ?>
</body>
</html>
