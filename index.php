<?php get_header(); ?>

	<?php

		global $query_string;
		query_posts( $query_string . '&posts_per_page=-1' );

        $title = get_the_title(9);
        $content = get_post_field('post_content', 9);

		if (have_posts()) { 
	?>

		<section class="page-section full"> 
			<section class="container ultra no-top">
				<section class="cols-1">
					<aside>
						<div class="column-row image_block ">
							<img src="<?php echo get_theme_mod('idri_archive_banner'); ?>" alt="News &amp; Views;">
						</div>						
					</aside>
				</section>
			</section>
		</section>

		<section class="page-section">
			<section class="container ultra no-top">

				<div class="flex-article">
					<aside class="page-main">
						<h1><?php echo $title; ?></h1>
						<div class="blog-list">
							<?php while ( have_posts() ) { ?>
								<?php the_post(); ?>
								<div class="post-excerpt">
									<aside class="excerpt-content">
										<span class="cat-list"><?php the_category( '' ); ?></span>
										<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										<p class="excerpt-meta">
											<span><?php the_author_posts_link(); ?></span>
											<span><?php the_time('F j<\s\up>S</\s\up>, Y'); ?></span>
										</p>
										<?php the_excerpt(); ?>
										<a href="<?php the_permalink(); ?>">Read more <i class="fa fa-angle-right"></i></a>
									</aside>
									<aside class="excerpt-thumb">
										<a href="<?php the_permalink(); ?>" class="read-more"><?php the_post_thumbnail('small-square'); ?></a>
									</aside>
								</div>
							<?php } ?>
						</div>
					</aside>
					<aside class="page-sidebar">
						<?php get_sidebar(); ?>
					</aside>
				</div>
			</section>
		</section>

	<?php } ?>

<?php get_footer(); ?>
